package extract;

import java.io.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Extraction {

    private String url;
    private String username;
    private String password;
    private Connection connexion;
    private HashMap<String, String> dictionary;

    /**
     * Constructeur de la classe d'extraction
     * @param url Url de la base de donnée
     */
    public Extraction(String url) {
        this.url = url;
        this.username = null;
        this.password = null;
        this.connexion = null;
        initDictionnary();
    }

    /**
     * Constructeur de la classe d'extraction avec nom d'utilisateur et mot de passe
     * @param url Url de la base de donnée
     * @param username Nom d'utilisateur de la base de donnée
     * @param password Mot de passe de la base de donnée
     */
    public Extraction(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.connexion = null;
        initDictionnary();
    }

    /**
     * Permet d'initialiser le dictionnaire des mots à exclure
     */
    private void initDictionnary(){
        dictionary = new HashMap<>();
        dictionary.put("ON", "");
        dictionary.put("OFF", "");
        dictionary.put("begin", "");
        dictionary.put("end", "");
        dictionary.put("OPEN", "");
        dictionary.put("CLOSE", "");
    }

    /**
     * Insère un fichier en base de donnée
     * @param fichier Le fichier à insérer en base
     * @return Retourne le fichier inséré avec l'id généré par la base
     */
    public Fichier insertionFichier(Fichier fichier){
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connexion.prepareStatement("INSERT INTO Fichier(nom) VALUES(?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, fichier.getNom());
            int nblignes = preparedStatement.executeUpdate();   // Lance la requête
            if(nblignes == 0){  // Si le nombre de ligne insérée est égale à 0 alors la requête s'est mal passée
                throw new SQLException("La création à échouée, aucune ligne n'a été créé.");
            }
            ResultSet resultat = preparedStatement.getGeneratedKeys();  // On récupère les données insérées
            if(resultat.next()){
                fichier.setId(resultat.getLong(1)); // On récupère l'id de l'objet inséré
            }
            else{   // Si aucun id n'est récupéré, la requête s'est mal passée
                throw new SQLException("La création à échouée, aucune ligne n'a été créé.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            fichier = null;
        }
        catch (NullPointerException e){
            e.printStackTrace();
            System.err.println("La connexion n'a probablement pas été établie");
            fichier = null;
        }
        return fichier;
    }

    /**
     * Insére une donnée en base de donnée
     * @param donnee La donnée à insérer
     * @return Retourne la donnée insérée avec l'id généré par la base
     */
    public Donnee insertionDonnee(Donnee donnee){
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connexion.prepareStatement("INSERT INTO Donnee(nom_activite, date_debut, date_fin, id_fichier) VALUES(?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, donnee.getNom());
            preparedStatement.setTimestamp(2, donnee.getDatedebut());
            preparedStatement.setTimestamp(3, donnee.getDatefin());
            preparedStatement.setLong(4, donnee.getFichier().getId());
            int nblignes = preparedStatement.executeUpdate();   // Lance la requête
            if (nblignes == 0) {    // Si le nombre de ligne insérée est égale à 0 alors la requête s'est mal passée
                throw new SQLException("La création à échouée, aucune ligne n'a été créé.");
            }
            ResultSet resultat = preparedStatement.getGeneratedKeys();  // On récupère les données insérées
            if (resultat.next()) {
                donnee.setId(resultat.getLong(1));  // On récupère l'id de l'objet inséré
            } else {    // Si aucun id n'est récupéré, la requête s'est mal passée
                throw new SQLException("La création à échouée, aucune ligne n'a été créé.");
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            donnee = null;
        }
        catch (NullPointerException e){
            e.printStackTrace();
            System.err.println("La connexion n'a probablement pas été établie");
            donnee = null;
        }
        return donnee;
    }

    /**
     * Insère plusieurs données en base, si une erreur ce produit, on annule les insertions
     * @param donnees Les données à inséré en base
     * @return Les donnees avec leurs id généré par la base
     */
    public Collection<Donnee> insertionDonnee(Collection<Donnee> donnees) {
        Donnee temp;
        try {
            connexion.setAutoCommit(false); // On enlève l'autocommit
            for(Donnee d : donnees){    // Pour chaque données, on les insére en base
                temp = insertionDonnee(d);
                d.setId(temp.getId());
            }
            connexion.commit(); // On valide le tout à la fin des insertions
        }
        catch (NullPointerException e){
            e.printStackTrace();
            System.err.println("La connexion n'a probablement pas été établie");
            donnees = null;
        }
        catch (SQLException e){
            try {
                connexion.rollback();   // Si une insertion s'est mal passée, on retourne en arrière
            }
            catch (SQLException ex){
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        try {
            connexion.setAutoCommit(true);  // On remet l'autocommit à la fin
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return donnees;
    }

    /**
     * Fonction de connexion à la base de donnée
     */
    public void connexion(){
        try {
            if(username == null)    // Si un username est présent, on se connecte avec sinon on se connecte juste avec l'url
                connexion = DriverManager.getConnection(url);
            else
                connexion = DriverManager.getConnection(url, username, password);
        }catch (SQLException e){
            e.printStackTrace();
            System.err.println("Erreur pendant la connexion à la base de donnée.");
        }
        System.out.println("Connexion établie.");
    }

    /**
     * Déconnexion de la base de donnée
     */
    public void deconnexion(){
        try {
            connexion.close();  // On ferme la connexion
            connexion = null;
        }catch (SQLException e){
            e.printStackTrace();
            System.err.println("Erreur pendant la fermeture de la connexion.");
        }
        System.out.println("Connexion fermée.");
    }

    /**
     * data.Extraction des données du fichier passé en paramètre. Les données sont directement stocké en base
     * @param fichier Chemin du fichier
     */
    public void extraireDonnee(String fichier){
        String ligne;
        BufferedReader br;
        String[] data;
        Timestamp date;
        Fichier f = new Fichier(fichier);
        ArrayList<Donnee> donnees = new ArrayList<>();
        try{
            br = new BufferedReader(new FileReader(fichier));
            while((ligne = br.readLine()) != null){ // Pour chaque ligne du fichier
                data = ligne.trim().split("\\s+");  // On fait un tableau pour chaque élement de la ligne
                if(ligne.contains("begin")){  // Si c'est le début on ajoute une nouvelle donnée
                    date = Timestamp.valueOf(LocalDateTime.parse((data[0] + " " + data[1]).replace(" ", "T")));
                    ligne = getActivite(ligne); // On récupère le nom de l'activité
                    donnees.add(new Donnee(ligne, date, f));
                }
                else if(ligne.contains("end")){   // Si c'est la fin on ajoute la date de fin
                    date = Timestamp.valueOf(LocalDateTime.parse((data[0] + " " + data[1]).replace(" ", "T")));
                    ligne = getActivite(ligne); // On récupère le nom de l'activité
                    for (int i = donnees.size()-1; i >= 0 ; i--) {
                        if(donnees.get(i).getNom().equals(ligne)){
                            donnees.get(i).setDatefin(date);
                            break;
                        }
                    }
                }
            }
            br.close();
            donnees = suppressionBruit(donnees);
            this.connexion();   // On se connecte à la base
            this.insertionFichier(f);   // On insére le fichier
            this.insertionDonnee(donnees);  // ON insére les données
            this.deconnexion(); // On se déconnecte
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
            System.err.println("data.Fichier non trouvé.");
        }
        catch (IOException e){
            e.printStackTrace();
            System.err.println("Erreur pendant la lecture du fichier.");
        }
    }

    /**
     * Récupère l'activité d'une donnée
     * @param ligne Ligne à analysé
     * @return Activité de la donnée
     */
    private String getActivite(String ligne){
        Pattern p = Pattern.compile("[0-9A-Z]{4}|[0-9]{1,2}\\.[0-9]|[0-9]{1,2}"); // Regex des capteurs
        Matcher m;
        String[] date = ligne.trim().split("\\s+"); // On récupère la date de la donnée
        for (Map.Entry<String, String> entry : dictionary.entrySet()) { // On enlève tout les élements du dictionnaire
            ligne = ligne.replace(entry.getKey(), entry.getValue());
        }
        ligne = ligne.replace(date[0] + " " + date[1], "").replaceAll("\\s+","");   // On enlève les dates et les espaces
        m = p.matcher(ligne);
        while(m.find())
            ligne = ligne.replace(m.group(), "");  // On enlève le capteur
        return ligne;
    }

    /**
     * Permet de récupérer un fichier grâce à son id
     * @param id Id du fichier
     * @return data.Fichier à récupéré
     */
    public Fichier getFichier(int id){
        PreparedStatement preparedStatement;
        Fichier fichier = null;
        try {
            preparedStatement = connexion.prepareStatement("SELECT * FROM Fichier WHERE id_fichier = (?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, id);
            ResultSet resultat = preparedStatement.executeQuery();   // Lance la requête
            if(resultat.next()){
                fichier = new Fichier(resultat.getInt(1), resultat.getString(2));
            }
            else{   // Si aucun id n'est récupéré, la requête s'est mal passée
                throw new SQLException("La récupération à échouée, aucune ligne n'a été récupérée.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e){
            e.printStackTrace();
            System.err.println("La connexion n'a probablement pas été établie");
        }
        return fichier;
    }

    /**
     * Récupère une donnée identifiée par son id
     * @param id Id de la donnée
     * @return Donnée à récupéré
     */
    public Donnee getDonnee(int id){
        PreparedStatement preparedStatement;
        Donnee donnee = null;
        try {
            preparedStatement = connexion.prepareStatement("SELECT * FROM Donnee WHERE id_donnee = (?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, id);
            ResultSet resultat = preparedStatement.executeQuery();   // Lance la requête
            if(resultat.next()){
                donnee = new Donnee(resultat.getInt(1), resultat.getTimestamp(2), resultat.getTimestamp(3), getFichier(resultat.getInt(4)), resultat.getString(5));
            }
            else{   // Si aucun id n'est récupéré, la requête s'est mal passée
                throw new SQLException("La récupération à échouée, aucune ligne n'a été récupérée.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e){
            e.printStackTrace();
            System.err.println("La connexion n'a probablement pas été établie");
        }
        return donnee;
    }

    /**
     * Récupère les données de la base et les mets dans une collection de donnée
     * @param f data.Fichier qui contient les données
     * @return Collection de donnée
     */
    public ArrayList<Donnee> getDonnee(Fichier f){
        PreparedStatement preparedStatement;
        ArrayList<Donnee> donnee = new ArrayList<>();
        try {
            preparedStatement = connexion.prepareStatement("SELECT * FROM Donnee WHERE id_fichier = (?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, f.getId());
            ResultSet resultat = preparedStatement.executeQuery();   // Lance la requête
            while (resultat.next()){
                donnee.add(new Donnee(resultat.getInt(1), resultat.getTimestamp(2), resultat.getTimestamp(3), f, resultat.getString(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e){
            e.printStackTrace();
            System.err.println("La connexion n'a probablement pas été établie");
        }
        return donnee;
    }

    /**
     * Récupère les données de la base et les mets dans une collection de donnée
     * @param f data.Fichier qui contient les données
     * @param s Chaine de caractère dont le nom de la donnée doit correspondre
     * @return Collection de donnée
     */
    public ArrayList<Donnee> getDonnee(Fichier f, String s){
        PreparedStatement preparedStatement;
        ArrayList<Donnee> donnee = new ArrayList<>();
        try {
            preparedStatement = connexion.prepareStatement("SELECT * FROM Donnee WHERE id_fichier = (?) and nom_activite LIKE (?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, f.getId());
            preparedStatement.setString(2,  "%" + s + "%");
            ResultSet resultat = preparedStatement.executeQuery();   // Lance la requête
            while (resultat.next()){
                donnee.add(new Donnee(resultat.getInt(1), resultat.getTimestamp(2), resultat.getTimestamp(3), f, resultat.getString(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e){
            e.printStackTrace();
            System.err.println("La connexion n'a probablement pas été établie");
        }
        return donnee;
    }

    /**
     * Supprime le bruit dans les données : Si une donnée se répète et que l'intervalle entre les deux données est de
     * moins de 15 minutes, alors on la considère comme une seule est même donnée.
     * @param data Liste de données à traité
     * @return Liste de données sans le bruit
     */
    private ArrayList<Donnee> suppressionBruit(ArrayList<Donnee> data){
        ArrayList<Donnee> resultat = new ArrayList<>();
        boolean find;
        long duree;
        for (Donnee donnee : data) {
            find = false;
            for(int i = 0; i < resultat.size(); i++){
                duree = donnee.getDatedebut().getTime() - resultat.get(i).getDatefin().getTime();
                if (donnee.getNom().equals(resultat.get(i).getNom()) && duree < 900000 && duree > 0) {
                    resultat.get(i).setDatefin(donnee.getDatefin());
                    find = true;
                    break;
                }
            }
            if(!find){
                resultat.add(donnee);
            }
        }
        return resultat;
    }
}
