package extract;

import java.sql.Timestamp;
import java.util.ArrayList;

public class Donnee implements Comparable{

    private long id;
    private Timestamp datedebut;
    private Timestamp datefin;
    private String nom;
    private Fichier fichier;

    /**
     * Constructeur de la donnee
     * @param id
     * @param datedebut
     * @param datefin
     * @param nom
     * @param fichier
     */
    public Donnee(long id, Timestamp datedebut, Timestamp datefin, Fichier fichier, String nom){
        this.setId(id);
        this.setDatedebut(datedebut);
        this.setDatefin(datefin);
        this.setNom(nom);
        this.setFichier(fichier);
    }

    /**
     * Constructeur de la donnee si on a pas l'id de la donnee
     * @param nom
     * @param datedebut
     * @param datefin
     * @param fichier
     */
    public Donnee(String nom, Timestamp datedebut, Timestamp datefin, Fichier fichier){
        this.setId(-1);
        this.setDatedebut(datedebut);
        this.setDatefin(datefin);
        this.setNom(nom);
        this.setFichier(fichier);
    }

    /**
     * Constructeur de la donner si on a pas la date de fin de la donnee
     * @param nom
     * @param datedebut
     * @param fichier
     */
    public Donnee(String nom, Timestamp datedebut, Fichier fichier){
        this.setId(-1);
        this.setDatedebut(datedebut);
        this.setDatefin(null);
        this.setNom(nom);
        this.setFichier(fichier);
    }

    /**
     * Récupération de l'id
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * setId
     * @param id Id de la donnee
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Récupération de la date de début
     * @return date de début
     */
    public Timestamp getDatedebut() {
        return datedebut;
    }

    /**
     * setDatedebut
     * @param datedebut Date de début de la donnée
     */
    public void setDatedebut(Timestamp datedebut) {
        this.datedebut = datedebut;
    }

    /**
     * Récupération de la date de fin de la donnée
     * @return date de fin
     */
    public Timestamp getDatefin() {
        return datefin;
    }

    /**
     * setDateFin
     * @param datefin Date de fin de la donnée
     */
    public void setDatefin(Timestamp datefin) {
        this.datefin = datefin;
    }

    /**
     * Récupération du nom de la donnée
     * @return nom de la donnée
     */
    public String getNom() {
        return nom;
    }

    /**
     * setNom
     * @param nom Nom de la donnée
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Récupération du fichier
     * @return data.Fichier qui contient la donnée
     */
    public Fichier getFichier() {
        return fichier;
    }

    /**
     * setFichier
     * @param fichier data.Fichier qui contient la donnée
     */
    public void setFichier(Fichier fichier) {
        this.fichier = fichier;
    }

    /**
     * Permet de calculer la durée entre le début et la fin
     * @return La durée entre la date de début et celle de fin
     */
    public double getDuree(){
        if(datefin != null && datedebut !=null)
            return this.getHeureFin() - this.getHeureDebut();
        return -1;
    }

    public double distance(Donnee d){
        return Math.sqrt(Math.pow(this.getDuree() - d.getDuree(), 2) + Math.pow(this.getHeureDebut() - d.getHeureDebut(), 2));
    }

    public double distanceNormalize(Donnee d, double maxDuree, double maxHeure){
        return Math.sqrt(Math.pow(this.getDuree()/maxDuree - d.getDuree()/maxDuree, 2) + Math.pow(this.getHeureDebut()/maxHeure - d.getHeureDebut()/maxHeure, 2));
    }

    public double getHeureDebut(){
        return (double)this.getDatedebut().getHours() + (double)this.getDatedebut().getMinutes()/60 + (double)this.getDatedebut().getSeconds()/3600;
    }

    public double getHeureFin(){
        return (double)this.getDatefin().getHours() + (double)this.getDatefin().getMinutes()/60 + (double)this.getDatefin().getSeconds()/3600;
    }

    /**
     * Fonction toString
     * @return Chaine regroupant le nom, les dates et la durée
     */
    public String toString(){
        return this.nom + " " + this.getDatedebut() + " " + this.getDatefin() + ", Durée : " + this.getDuree();
    }

    public boolean equals(Object o){
        if(o instanceof Donnee){
            if(this.nom.equals(((Donnee) o).nom) && this.id == ((Donnee) o).id) {
                return this.datedebut.equals(((Donnee) o).datedebut) && this.datefin.equals(((Donnee) o).datefin);
            }
        }
        return false;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Donnee){
            if(this.getDuree() > ((Donnee) o).getDuree()){
                return 1;
            }
            else if(this.getDuree() < ((Donnee) o).getDuree()){
                return -1;
            }
            return 0;
        }
        return Integer.MIN_VALUE;
    }
}
