package extract;

import kmeans.Kmeans;
import xmeans.Xmeans;
import javax.swing.*;

public class Main extends JPanel{
    public static void main(String[] args) {
        Extraction e = new Extraction("jdbc:sqlite:donnees.db");
        e.connexion();
        var fichier = e.getFichier(68);
        var donnees = e.getDonnee(fichier, "Eating");
        donnees.removeIf(c -> c.getDuree() < 0);
        e.deconnexion();
        var xmeans = new Xmeans(2, 50, donnees);
        xmeans.execute();
        xmeans.affichage();
    }
}
