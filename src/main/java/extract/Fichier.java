package extract;

public class Fichier{

    private long id;
    private String nom;

    /**
     * Constructeur de la classe fichier
     * @param id Id du fichier
     * @param nom Nom du fichier
     */
    public Fichier(long id, String nom) {
        setId(id);
        setNom(nom);
    }

    /**
     * Second constructeur si on n'a pas l'id
     * @param nom Nom du fichier
     */
    public Fichier(String nom) {
        setId(-1);
        setNom(nom);
    }

    /**
     * Récupération de l'id
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * setId
     * @param id Id du fichier
     */
    public void setId(long id){
        this.id = id;
    }

    /**
     * Récupération du nom
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * setNom
     * @param nom Nom du fichier
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

}

