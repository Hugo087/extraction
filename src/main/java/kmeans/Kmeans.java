package kmeans;

import extract.Donnee;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;

import java.util.ArrayList;
import java.util.Random;

public class Kmeans {

    private ArrayList<Cluster> clusters;
    private ArrayList<Donnee> donnees;

    /**
     * Constructeur de KMeans
     * @param nbCluster Le nombre de cluster voulu
     * @param donnees Les données à traité
     */
    public Kmeans(int nbCluster, ArrayList<Donnee> donnees){
        this.clusters = new ArrayList<>(nbCluster);
        this.donnees = donnees;
        createCluster(nbCluster);
    }

    /**
     * Création des clusters, on choisi aléatoirement les centroid
     * @param nbCluster Le nombre de cluster à créer
     */
    private void createCluster(int nbCluster){
        Random rand = new Random();
        int number;
        for(int i = 0; i < nbCluster; i++){
            number = rand.nextInt(donnees.size());
            this.clusters.add(new Cluster(donnees.get(number)));
        }
        reatributeCluster();
    }

    /**
     * Démarre K-means
     */
    public void execute(){
        ArrayList<Donnee> precedent = new ArrayList<>(clusters.size());
        boolean fini;
        do{
            precedent.clear();
            clusters.forEach((e) -> precedent.add(e.getCentroid()));
            fini = true;
            clusters.forEach(Cluster::centralize);
            reatributeCluster();
            for(int i = 0; i < clusters.size(); i++){
                if(!clusters.get(i).getCentroid().equals(precedent.get(i))){
                    fini = false;
                }
            }
        }while(!fini);
    }

    /**
     * Attribue chaque donnée à un cluster
     */
    private void reatributeCluster(){
        double number, current, maxDuree = 0, maxHeure = 0;
        int cent;
        clusters.forEach(Cluster::resetData);
        for (Donnee d: this.donnees) {
            if(d.getDuree() > maxDuree) maxDuree = d.getDuree();
            if(d.getHeureDebut() > maxHeure) maxHeure = d.getHeureDebut();
        }
        for (int i = 0; i < donnees.size(); i++) {
            number = Double.MAX_VALUE;
            cent = 0;
            for(int j = 0; j < clusters.size(); j++){
                current = donnees.get(i).distanceNormalize(clusters.get(j).getCentroid(), maxDuree, maxHeure);
                if(current < number){
                    cent = j;
                    number = current;
                }
            }
            clusters.get(cent).addData(donnees.get(i));
        }
    }


    public String toString(){
        return clusters.toString();
    }

    /**
     * Récupération des clusters
     * @return Clusters à récupérer
     */
    public ArrayList<Cluster> getClusters(){
        return this.clusters;
    }

    /**
     * Permet de définir les clusters
     * @param clusters
     */
    public void setClusters(ArrayList<Cluster> clusters){
        this.clusters = clusters;
    }

    /**
     * Récupération des données
     * @return Permet de récupérer les données
     */
    public ArrayList<Donnee> getDonnees(){
        return this.donnees;
    }

    /**
     * Permet de récupérer les noms des différentes données
     * @return La listes des données des données
     */
    public ArrayList<String> getNameOfDifferentData(){
        ArrayList<String> names = new ArrayList<>();
        for (Donnee d: this.donnees) {
            if(!names.contains(d.getNom())) names.add(d.getNom());
        }
        return names;
    }

    /**
     * Affiche les données
     */
    public void affichage() {
        ArrayList<String> names = this.getNameOfDifferentData();
        double[] x;
        double[] y;
        final XYChart chart = new XYChartBuilder().width(800).height(600).build();
        int j = 0;
        for (Cluster c : this.getClusters()) {
            x = new double[c.getData().size()];
            y = new double[c.getData().size()];
            for(int i = 0; i < c.getData().size(); i++){
                x[i] = (double)c.getData().get(i).getDatedebut().getHours() + (double)c.getData().get(i).getDatedebut().getMinutes()/60 + (double)c.getData().get(i).getDatedebut().getSeconds()/3600;
                y[i] = c.getData().get(i).getDuree();
            }
            if(x.length != 0)
            chart.addSeries("Cluster " + j, x, y);
            j++;
        }

        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Scatter);
        chart.getStyler().setChartTitleVisible(false);
        chart.getStyler().setMarkerSize(6);
        final SwingWrapper<XYChart> sw = new SwingWrapper<>(chart);
        sw.displayChart();
    }
}
