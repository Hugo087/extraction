package kmeans;

import extract.Donnee;

import java.util.ArrayList;

public class Cluster{

    private ArrayList<Donnee> data;
    private Donnee centroid;

    /**
     * Constructeur du cluster
     */
    public Cluster(){
        this.data = new ArrayList<>();
        this.centroid = null;
    }

    /**
     * Constructeur du cluster avec centroid défini
     * @param centroid
     */
    public Cluster(Donnee centroid){
        this();
        this.centroid = centroid;
    }

    /**
     * Calcul du centroid des données
     */
    public void centralize(){
        double sommeDuree = 0;
        double sommeDebut = 0;
        double moyenneDuree = 0;
        double moyenneDebut = 0;
        double distanceDonnee = 0;
        double distanceCentroid = 0;
        double maxDuree = 0;
        double maxHeure = 0;
        for(Donnee d : data){
            if(d.getDuree() > maxDuree) maxDuree = d.getDuree();
            if(d.getHeureDebut() > maxHeure) maxHeure = d.getHeureDebut();
        }
        for(Donnee d : data){
            sommeDuree += d.getDuree()/maxDuree;
            sommeDebut += d.getHeureDebut()/maxHeure;
        }
        if(data.size() != 0) {
            moyenneDuree = sommeDuree / data.size();
            moyenneDebut = sommeDebut / data.size();
        }
        for (Donnee d: data) {
            if(centroid != null) {
                distanceDonnee = Math.sqrt(Math.pow(d.getDuree()/maxDuree - moyenneDuree, 2) + Math.pow(d.getHeureDebut()/maxHeure - moyenneDebut, 2));
                distanceCentroid = Math.sqrt(Math.pow(centroid.getDuree()/maxDuree - moyenneDuree, 2) + Math.pow(centroid.getHeureDebut()/maxHeure - moyenneDebut, 2));
            }
            if(centroid == null || distanceDonnee < distanceCentroid){
                centroid = d;
            }
        }
    }

    /**
     * Récupération du centroid
     * @return Centroid du cluster
     */
    public Donnee getCentroid(){
        return centroid;
    }

    /**
     * Permet de rajouter une donnée au cluster
     * @param d Donnée à ajouter
     */
    public void addData(Donnee d){
        this.data.add(d);
    }

    /**
     * Récupération des données
     * @return Données du cluster
     */
    public ArrayList<Donnee> getData(){
        return data;
    }

    public String toString(){
        return "Centroid : " + centroid.toString() + "\nDonnees : " + data.toString() + "\n";
    }

    /**
     * Permet d'effacer les données du cluster
     */
    public void resetData(){
        this.data = new ArrayList<>();
    }

}
