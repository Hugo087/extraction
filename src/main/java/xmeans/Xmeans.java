package xmeans;

import extract.Donnee;
import kmeans.Cluster;
import kmeans.Kmeans;
import java.util.ArrayList;
import java.util.Collections;

public class Xmeans extends Kmeans{

    private final int borneinf;
    private final int bornesup;

    /**
     * Constucteur de Xmeans
     * @param borneinf Le nombre de cluster avec lequel on commence
     * @param bornesup Le nombre maximal de cluster
     * @param donnees Les données à traiter
     */
    public Xmeans(int borneinf, int bornesup, ArrayList<Donnee> donnees){
        super(borneinf, donnees);
        this.borneinf = borneinf;
        this.bornesup = bornesup;
    }

    /**
     * Fonction de vraisemblance pour calculer le BIC
     * @param r Nombre de données du premier cluster
     * @param rn Nombre de données d'un cluster après le split
     * @param var Variance
     * @param k Nombre de cluster
     * @param m Nombre de données total
     * @return
     */
    private double loglikelihood(double r, double rn, double var, double k, double m){
        double c1 = -rn/2 * Math.log(2 * Math.PI);
        double c2 = -(rn * m)/2 * Math.log(var);
        double c3 = -(rn - k)/2;
        double c4 = rn * Math.log(rn);
        double c5 = -rn * Math.log(r);
        return c1 + c2 + c3 + c4 + c5;
    }

    /**
     * Execute x-means
     */
    public void execute(){
        int rn, r;
        double sum;
        double var;
        Kmeans old_kmeans;
        Kmeans new_kmeans;
        int k = borneinf;
        int m = super.getDonnees().size();
        int p, k_temp, old_k;
        double[] old_bic;
        double new_bic;
        Cluster cluster;
        do {
            // Improve params
            old_k = k;
            old_kmeans = new Kmeans(k, super.getDonnees()); // On commence par faire kmeans
            old_kmeans.execute();
            // Improve structure
            k_temp = 0;
            old_bic = new double[k];
            for (int i = 0; i < k - 1; i++) {
                cluster = old_kmeans.getClusters().get(i);
                sum = 0;
                rn = cluster.getData().size();  // Nombre de donnée dans un cluster
                for (Donnee d : cluster.getData()) {
                    sum += Math.pow(d.getDuree() - cluster.getCentroid().getDuree(), 2); // On additionne les durées pour la formule de la variance
                }
                var = sum / (rn - k);   // Fonction de la variance
                p = m;
                old_bic[i] = loglikelihood(rn, rn, var, k, m) - (double) p / 2 * Math.log(rn);  // On calcul le BIC de chaque cluster
            }

            for (int i = 0; i < k - 1; i++) {
                cluster = old_kmeans.getClusters().get(i);
                new_kmeans = new Kmeans(2, cluster.getData());  // On divise chaque cluster en deux et on réexecuté k-means pour chaque division
                new_kmeans.execute();
                r = cluster.getData().size();   // Nombre de donnnée dans le cluster
                new_bic = 0;
                for (Cluster c : new_kmeans.getClusters()) {
                    rn = c.getData().size();    // Nombre de donnée dans le cluster divisé
                    sum = 0;
                    for (Donnee d : c.getData()) {
                        sum += Math.pow(d.getDuree() - c.getCentroid().getDuree(), 2);  // On additionne les durées pour la formule de la variance
                    }
                    var = sum / (rn - k);   // Calcul de la variance
                    new_bic += loglikelihood(r, rn, var, k, m); // On additionne les BIC de chaque sous cluster
                }
                p = 2 * m ;
                new_bic -= (double) p / 2 * Math.log(r); // On calcul le bic des sous-clusters
                if (old_bic[i] < new_bic) k_temp++; // Si le nouveau BIC est plus grand que l'ancien, on ajoute un cluster
            }
            k += k_temp;    // On ajoute les clusters
        }while(old_k != k && k <= bornesup);    // Si on a atteint la limite fixé ou si l'ancien k est le même que le nouveau, alors on sort sinon on recommence
        old_kmeans = new Kmeans(old_k, super.getDonnees()); // On execute kmeans avec le k trouvé
        old_kmeans.execute();
        super.setClusters(old_kmeans.getClusters());
    }

}
